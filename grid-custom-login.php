<?php
/*
 * Plugin Name: GRID Custom Login
 * Description: Add some custom CSS to wp-admin login page
 * Version: 0.1
 * Author: GRID Agency
 * Author URI: https://www.grid-agency.com
*/

if( !defined('ABSPATH') ) {
	die();
}

function grid_mu_custom_login(){
	
	global $client_logo;
	
	if( empty($client_logo) ) {
		$logo = WPMU_PLUGIN_URL . '/grid-custom-login/logo.png';
	} else {
		$logo = $client_logo;
	}
	
	$backgroundPage = '#f1f1f1';
	$backgroundForm = '#ffffff';
	
	$customLoginCSS = '<style type="text/css">
		
		body.login{
			background: ' . $backgroundPage . '!important;
		}
		.login form{
			background: ' . $backgroundForm . '!important;
		}
		.login h1 a {
			background: url(' . $logo . ') center center no-repeat !important;
			background-size: contain !important;
			width: calc(100% - 30px) !important;
			min-height: 150px !important;
		}
	</style>';

	echo $customLoginCSS;

}
add_action( 'login_enqueue_scripts', 'grid_mu_custom_login', 10 );